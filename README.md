# Blocklists maintained by Miss Support

Currently there are three host lists included in this repository. These lists are meant to be used as host lists for host based DNS blocking with an app like Blokada ([blokada.org](https://blokada.org)).\
Please note: these host lists need further validation and expansion, so please feedback your experiences and hosts that are missing.

### Google blocklist
I created a host list containing all Google domains that were requested by my phone as well as other tracking domains that weren't blocked. Blocking these domains can and will break many Google apps and services. It is meant as an add-on for people valuing their privacy and trying to avoid Google products while not using a custom ROM. So if you encounter blocked hosts that you need, either whitelist them (I try to group the domains in the file, so maybe check there which ones might be needed for an app to work) or search for alternative services or apps. I therefore recommend checking out these links:

*  [F-Droid](https://f-droid.org/),
*  [PrivacyTools](https://www.privacytools.io/),
*  [Libreware TG Channel](https://t.me/libreware),
*  [Kuketz-blog](https://www.kuketz-blog.de/) (german).

If you are using AuroraStore, bypass it, reboot and updates for apps should be working.

#### mtalk extension
This repo also provides an extension for 'mtalk' adresses. They are relevant for the Firebase Cloud Messaging services (formerly known as Google Cloud Messaging services), which are used for push notifications. Consider opting-in for this list wisely, as this affects the behavior of for example Whatsapp.
### Huawei blocklist
As pendant to Jerryn70's Xiaomi blocker list, we thought about starting a list specific for Huawei domains. To start that list I contacted Franc. The credit goes to him.\
As Huawei uses Avast and Swiftkey as build-in apps, we decided to add tracking domains from these services as well.\
If you use a Huawei phone, feel welcome to share your requests.csv file with me so that it can be checked for other domains specifically requested by Huawei.
